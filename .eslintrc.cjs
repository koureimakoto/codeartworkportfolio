 /* eslint-env node, es6 */
module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended",
        "plugin:security/recommended",
        "plugin:@typescript-eslint/recommended"
    ],
    "overrides": [
    ],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": "latest",
        "sourceType" : "module"
    },
    "plugins": [
        "react",
        "@typescript-eslint"
    ],
    "ignorePatterns": ["/node_modules/*"],
    "rules": {
    /* This is required to remove the need to import from react in all files  */
        "react/jsx-uses-react"    : "off",
        "react/react-in-jsx-scope": "off",
        "prefer-const": "off"
    }
}
