 /* eslint-env node, es6 */
import { defineConfig, splitVendorChunkPlugin } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), splitVendorChunkPlugin()],
  build: {
    outDir: "./portfolio/",
    rollupOptions: {
      output: {
        entryFileNames: "[name].js",
        chunkFileNames: "[name].[hash].js",
        assetFileNames: "[name].[hash].[ext]",
        /*manualChunks: (id) => {
          if(id.includes('node_modules'))
            if(id.includes('highlight.js'))
              return "vendor_hljs"
            return "vendor"
          }*/
        }
    }
  }
})
