import { Routes, Route, Route as RouteWrapper } from 'react-router-dom'
import { DefaultLayout } from './layouts/DefaultLayout'
import { Home }     from './pages/Home'
import { Codes }    from './pages/Codes'
import { Artworks } from './pages/Artworks'
import { Page404 } from './components/404'

export function
Router() : JSX.Element {
    const pages = {
        root: '/',
        error:'*',
        code: {
            root: '/codes',
        },
        artwork: {
            root: '/artworks'
        }
    }

    const comp : JSX.Element = (
        <Routes>
            <Route path={pages.root} element={<Home />} />

            <RouteWrapper path={pages.code.root}    element={<DefaultLayout />}>
                <Route path={pages.code.root} element={<Codes/>}/>
            </RouteWrapper>
            
            <RouteWrapper path={pages.artwork.root} element={<DefaultLayout />}>
                <Route path={pages.artwork.root} element={<Artworks/>}/>
            </RouteWrapper>
            
            <Route path={pages.error} element={<Page404 />}/>
        </Routes>
    )

    return comp
}