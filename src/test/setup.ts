/* eslint-disable */
import '@testing-library/jest-dom'

const console_bkp = global.console
const consoel_new = global.console = <any>{
    log: jest.fn(),
    debug: console.debug,
    error: console.error,
    trace: jest.fn(),
    group: jest.fn(),
    groupCollapsed: jest.fn()
}