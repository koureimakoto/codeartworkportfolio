export function
space( size: number, max_size: number ) {
    let space = ' '
    while( size < max_size ) {
        size  += 1
        space += ' '
    }
    return space
}


