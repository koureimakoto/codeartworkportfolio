import { Link } from 'react-router-dom';
import { d_main_code_menu } from '../../../components/Front/FrontDataTypes'
import { space } from './commons';

export function
PhpLayout( { items }: d_main_code_menu  ): JSX.Element {

    if( !items?.length )
    return <div> Empty Items for construct the Menu </div>

    /**
     * calculates the largest name
     */
    let longer = 0
    items.forEach( ( { name } ) => {
        if( name.length > longer )
            longer = name.length
    });
    // Add one more space for design reason
    longer += 1

    const comp = (
        <div className='specialCodeArea php' >
            <div ><span>{"public function"}</span></div>
            <div ><span>{"codeLayout( :o ) {"}</span></div>
            <div ><span>{"    switch( _ ) {"}</span></div>
            {// PHP SWITCH STATEMENT
                items.map( ( { to, name, className, callBackFunction } ) => (
                    <div key={ window.crypto.randomUUID() } >
                        <Link
                        className = { to + ' mainCodeMenu'}
                        onClick={ callBackFunction }
                        to={'/' + to}
                        >
                            <span>{"        case _ : { "}</span>
                            <span className={ className } >{
                                "$self::" +
                                name  + '();'+
                                space( name.length, longer )
                                }
                            </span>
                        </Link>
                        <span>{"break; },"}</span>
                    </div>
                ) )
            }
            <div>
                <Link to='/'>
                    <span>{"        default: "}</span>
                    <span className={items[0].className}>{"$self::Surprise();"}</span>
                </Link>
            </div>
            <div ><span>{"    }"}</span></div>
            <div ><span>{"}"}</span></div>
        </div>
    )

    return comp
}