import { Link }             from 'react-router-dom'
import { space }            from './commons'
import { d_main_code_menu } from '../../../components/Front/FrontDataTypes'

/**
 *  Rust Menu Layout
 */
export function
ClangLayout( { items }: d_main_code_menu ) {

    if( !items?.length )
        return <div> Empty Items for construct the Menu </div>


    let first = true;
    
    /**
     * calculates the largest name
     */
    let longer = 0
    items.forEach( ( { name } ) => {
        if( name.length > longer )
            longer = name.length
    });
    // Add one more space for design reason
    //longer += 1

    /**
     * This function return a Link Element using Pseudo C Language
     * ```tsx
     * cLangItem
     *  >> to       : string          // Query String
     *  >> name     : string          // Link Name
     *  >> className: string          // CSS Class Name
     *  >> callback : function (void) // Onlick Event Function
     *  << JSX.Element
     * ``` 
     */
    function
    cLangItem( to: string, name: string, cname: string, callback: () => void ) {
        let spc = '    else '
        if( first ) {
            spc = '    '
            first = false
        }

        const comp = (
            <>
            <div key={ window.crypto.randomUUID() } >
                <span>{ spc + "if( _ )"}</span>
            </div>
            <div key={ window.crypto.randomUUID() } >
                <Link
                className = { to + ' mainCodeMenu' }
                onClick   = { callback             }
                to        = { '/' + to             }
                >
                    <span className={ cname } >{
                        "        g_"       + 
                        name.toLowerCase() + 
                        "();"              + 
                        space( name.length, longer )
                        }
                    </span>
                </Link>
            </div>
            
            </>
        )

        return comp
    }

    /**
     * This function return the Last Link element as a suprise Path
     * ```tsx
     * cLangSurprise
     *  >> className: string          // CSS Class Name
     *  << JSX.Element
     * ``` 
     */
    function
    cLangSurprise( cname: string ) {
        const comp = (
            <>
            <div><span>{"    else"}</span></div>
            <div>
                <Link to='/'>
                    <span className={ cname }>{"        g_surprise();"}</span>
                </Link>
            </div>
            </>
        )

        return comp
    }

    const cont = (
        <div className='specialCodeArea clang'>
            <div ><span>{"void"}</span></div>
            <div ><span>{"main( :o ) {"}</span></div>
            <div ><span>{"    char *p = getenv(\"QUERY_STRING\");"}</span></div>
            <>
            { items.map(({ to, name, className, callBackFunction }) => (
                <>{cLangItem(to, name, className, callBackFunction )}</>
            ))}
            { cLangSurprise( items[0].className ) }
            </>
            <div ><span>{"}"}</span></div>
        </div>
    )

    return cont
}