import { d_main_code_menu } from "../../../components/Front/FrontDataTypes"
import { ClangLayout } from "./ClangLayoult"
import { PhpLayout } from "./PhpLayout"
import { RustLayout } from "./RustLayout"
import { TypeLayoult } from "./TypeLayout"

type randCode = { name: string, cont: JSX.Element } 

export function
CodeMenuLayout( props: d_main_code_menu ): JSX.Element {

    let codes: randCode[] = []

    codes.push( {
        name: 'x', 
        cont: <TypeLayoult items={ props.items } />
    } as randCode )

    codes.push( {
        name: 'y', 
        cont: <RustLayout items={ props.items } />
    } as randCode )

    codes.push( {
        name: 'y', 
        cont: <PhpLayout items={ props.items } />
    } as randCode )

    codes.push( {
        name: 'y', 
        cont: <ClangLayout items={ props.items } />
    } as randCode )

    function
    rand(min: number, max: number) {
        return Math.trunc( Math.random() * ( max - min ) + min )
    }

    const cont = (
        <>
        { codes[rand(0, codes.length)].cont }
        </>
    )

    return cont
} 