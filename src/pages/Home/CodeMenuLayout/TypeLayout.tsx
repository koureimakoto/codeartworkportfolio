import { Link } from "react-router-dom"
import { d_main_code_menu } from "../../../components/Front/FrontDataTypes"
import { space } from "./commons";

export function
TypeLayoult( { items } : d_main_code_menu ) {

    if( !items?.length )
        return (
            <div> Empty Item for construct Menu </div>
        )


    // -- Init 

    let longer = 0
    items.forEach( ({ name }) => {
        if( name.length > longer )
            longer = name.length
    });

    const tmp_cont = (
        <>
            <div className='specialCodeArea ts'>
            <div><span>{"export function"}</span></div>
            <div><span>{"codesLayout( :o ) {"}</span></div>
            <div><span>{"    return ("}</span></div>
            <div><span>{"    <Routes>"}</span></div>
            {
                items.map( ( { to, name, className, callBackFunction } ) => (                    
                    <div key={ window.crypto.randomUUID() } >
                        
                        <span>{"        <Route _=\"{"}</span>
                        <Link
                            className = { to + ' mainCodeMenu'}
                            onClick={ callBackFunction }
                            to={'/' + to}
                        ><span className={ className } >{
                                    " < "                 + 
                                    name                         +
                                    space( name.length, longer ) +
                                    "/>"
                                }
                            </span>
                        </Link>
                        <span>{ "} />"}</span>
                    </div>
                ) )
            }
            <div>
                <span>{"        <Route _=\"{"}</span>
                <Link to='/'>
                    <span className={items[0].className}>{
                        " < Surprise" + space( 'Surprise'.length, longer ) +
                        "/>"}
                    </span>
                </Link>
                <span>{ "} />"}</span>
            </div>
            <div><span >{"    </Routes>"}</span></div>
            <div><span>{"    )"}</span></div>
            <div><span>{"}"}</span></div>
        </div>
        </>
    )

    return tmp_cont
}