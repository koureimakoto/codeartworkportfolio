import { Link }             from 'react-router-dom'
import { space }            from './commons'
import { d_main_code_menu } from '../../../components/Front/FrontDataTypes'

/**
 *  Rust Menu Layout
 */
export function
RustLayout( { items }: d_main_code_menu ) {

    if( !items?.length )
        return <div> Empty Items for construct the Menu </div>

    
    /**
     * calculates the largest name
     */
    let longer = 0
    items.forEach( ( { name } ) => {
        if( name.length > longer )
            longer = name.length
    });
    // Add one more space for design reason
    


    const cont = (
        <div className='specialCodeArea rs'>
            <div ><span>{"pub fn"}</span></div>
            <div ><span>{"code_layout( :o ) {"}</span></div>
            <div ><span>{"    match _ {"}</span></div>
            {// Rust MATCH PATTERN
                items.map( ( { to, name, className, callBackFunction } ) => (                    
                    <div key={ window.crypto.randomUUID() } >
                        <Link
                        className = { to + ' mainCodeMenu'}
                        onClick={ callBackFunction }
                        to={'/' + to}
                        >
                            <span>{"        link::"}</span>
                            <span className={ className } >{
                                    name + space( name.length, longer )
                                }
                            </span>
                            <span>{"=> _ ,"}</span>
                        </Link>
                    </div>
                ) )
            }
        <div>
            <Link to='/'>
                <span>{"        _  => "}</span>
                <span className={items[0].className}>{"Surprise"}</span>
            </Link>
        </div>
        <div ><span>{"    }"}</span></div>
        <div ><span>{"}"}</span></div>
        </div>

    )

    return cont
}