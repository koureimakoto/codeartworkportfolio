// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FrontFooter as Footer } from   '../../components/Front/FrontFooter'
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FrontCenter as Center } from   '../../components/Front/FrontCenter'
import { FrontSide   as Left, FrontSide as Right }   from '../../components/Front/FrontSide/'

import './styles.pcss'
import { d_main_code_menu, d_side } from '../../components/Front/FrontDataTypes'
import { CodeMenuLayout } from './CodeMenuLayout'


export function
Home() : JSX.Element {

    const left_side : d_side = {
        to: 'codes',
        name: 'Codes Portfolio',
        className: 'homeCodeBtn ',
        callBackFunction: () => {
            //useState('leftClick')
            console.log('leftClick')
        }
    }

    const right_side : d_side = {
        to: 'artworks',
        name: 'Artworks Portfolio',
        className: 'homeArtworkBtn',
        callBackFunction: () => {
            //useState('rightClick')
            console.log('rightClick')
        }
    }


    const codeMenuData: d_main_code_menu = {
        items: [ {
                to: 'codes',
                name: 'Games',
                className: 'linkCode games',
                callBackFunction: () => {
                    console.log('games')
                }
            } as d_side , {
                to: 'codes',
                name: 'Layouts',
                className: 'linkCode layouts',
                callBackFunction: () => {
                    console.log('Layouts')
                }
            } as d_side , {
                to: 'codes',
                name: 'Softwares',
                className: 'linkCode softwares',
                callBackFunction: () => {
                    console.log('Softwares')
                }
            } as d_side , {
                to: 'codes',
                name: 'Tips',
                className: 'linkCode tips',
                callBackFunction: () => {
                    console.log('Tips')
                }
            } as d_side ,
        ]
    }


    const comp : JSX.Element = (
        <nav className='homeMenu'>
            
            < CodeMenuLayout items= {codeMenuData.items} />

            <Left  
                to={left_side.to}
                name={left_side.name} 
                className={left_side.className}
                callBackFunction={left_side.callBackFunction}
            />
            <Right 
                to={right_side.to}
                name={right_side.name} 
                className={right_side.className}
                callBackFunction={right_side.callBackFunction}
            />
        </nav>
    )

    return comp
}
