import { Link    } from 'react-router-dom'
import { d_side  } from '../FrontDataTypes'

import './styles.pcss'

export function
FrontSide(props : d_side) : JSX.Element {
    const comp : JSX.Element = (
        <Link 
            className={props.to + ' homeNavSide'}
            onClick={props.callBackFunction}
            to={'/'+props.to} 
        >   <div>
                <h1> {props.name} </h1>
            </div>
        </Link>
    )

    return comp
}