export type d_side = {
    to: string,
    name: string,
    extra?: string,
    className: string,
    callBackFunction: () => void
}

export type d_main_code_menu = {
    items: d_side[];
}