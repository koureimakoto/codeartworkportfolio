import { render } from '@testing-library/react'
import { FrontFooter as Footer } from './FrontFooter'

describe('Home Footer Components', () => {

    it('Copyright Mark Fouded', () => {
        const { getByText } = render(<Footer />)
        expect(
            getByText('© 2022 Talles Fagundes')
        ).toBeInTheDocument()
    })

    it('Check Class Name', () => {
        const { container } = render(<Footer />)
        const comp = container.getElementsByClassName('makotocopyright')
        expect(
            comp.length
        ).toBe(1)
    })

})