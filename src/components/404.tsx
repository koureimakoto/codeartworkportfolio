export function
Page404() : JSX.Element {
    const comp : JSX.Element = (
        <section className='home404'>
            <h1>Returned code 404</h1>
            <h2>Not Found</h2>
        </section>
    )

    return comp
}