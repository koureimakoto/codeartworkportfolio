import { Outlet } from 'react-router-dom'
import { Header } from '../components/Header'

export function
DefaultLayout() : JSX.Element {
    const comp : JSX.Element = (
        <>
        <Header />
        <Outlet />
        </>
    )

    return comp
}