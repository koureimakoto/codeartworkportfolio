// eslint-disable-next-line @typescript-eslint/no-unused-vars
/* React Only */
import React from 'react'
import ReactDOM from 'react-dom/client'

/* Extern Only */
import { BrowserRouter } from 'react-router-dom'

/* Intern Only */
import App  from './App'

const root = ReactDOM.createRoot(
  /* as HTMLElement to force caste */
  document.querySelector("#root") as HTMLElement
)

const comp = (

    <BrowserRouter>
          <App/>
    </BrowserRouter>     

)

root.render(comp)